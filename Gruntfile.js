module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
            options: {
                style: "compressed"
            },
            css: {
                files: [{
                    expand: true,
                    cwd: 'public/css', 
                    src: '*.scss',
                    dest: 'public/css',
                    ext:  '.css'
                }]
            }
        },
		watch: {
			sass: {
                files: ['public/css/**/*scss'],
                tasks: ['sass'],
                options: {
                    interrupt: true,
                    atBegin: true
                }
            }
		}
	});
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default',['watch']);
}