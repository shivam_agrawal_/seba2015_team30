package models;

import play.data.validation.Constraints;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
public class PropertyType {

    @Id
    @GeneratedValue
    public Integer id;

    @Constraints.Required
    public String name;

    @OneToMany(mappedBy = "type")
    public Collection<Property> properties;


    public static List<PropertyType> findAll() {

        TypedQuery<PropertyType> query = JPA.em().createQuery("SELECT g FROM PropertyType g", PropertyType.class);
        return query.getResultList();
    }

    public static PropertyType findById(Integer id) {
        return JPA.em().find(PropertyType.class, id);
    }

    public void save() {

        JPA.em().persist(this);
    }


}
