package controllers;

import models.Property;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;


public class PropertyActions extends Controller {

//    @Transactional(readOnly = true)
//    public static Result showAll() {
//
//        List<Property> allMovies = Property.findAll();
//
//        return ok(showAll.render(allMovies));
//
//    }
//
//    @Transactional(readOnly = true)
//    public static Result addNew() {
//        String successMessage = flash("success");
//        return ok(addNew.render(PropertyType.findAll(), successMessage));
//    }
//
//    @Transactional
//    public static Result createMovie() {
//        DynamicForm form = Form.form().bindFromRequest();
//
//        int genreId = Integer.parseInt(form.get("type-id"));
//        PropertyType type;
//        if (genreId == 0) {
//            type = new PropertyType();
//            type.name = form.get("type-name");
//            type.save();
//        } else {
//            type = PropertyType.findById(genreId);
//        }
//
//        String movieName = form.get("name");
//        Property movie = new Property();
//        movie.type = type;
//        movie.title = movieName;
//        movie.save();
//        flash("success", "Eintrag erfolgreich gespeichert");
//        return redirect(controllers.routes.PropertyActions.addNew());
//    }
//
//
//    @Transactional(readOnly = true)
//    public static Result showGenre(int genreId) {
//
//        if (genreId == 0) {
//            genreId = (session("last_genre_id") != null) ? new Integer(session("last_genre_id")) : 1;
//        } else {
//            session("last_genre_id", genreId + "");
//        }
//        PropertyType type = PropertyType.findById(genreId);
//        Collection<Property> movies = type.movies;
//        List<Property> moviesList = (List) movies;
//        List<PropertyType> allGenres = PropertyType.findAll();
//
//        return ok(showGenre.render(type, allGenres, moviesList));
//    }

    public static Result index() {
        return ok(views.html.main.render());
    }




    @Transactional
    public static Result createMovie() {
        Property newProperty = Json.fromJson(request().body().asJson(), Property.class);
        newProperty.save();
        return created(Json.toJson(newProperty));

    }


    @Transactional
    public static Result updateMovie(int id) {
        Property updatedProperty = Json.fromJson(request().body().asJson(), Property.class);
        updatedProperty.id = id;
        Property updated = updatedProperty.update();
        return ok(Json.toJson(updated));

    }


    @Transactional
    public static Result deleteMovie(int id) {
        Property.findById(id).delete();
        return noContent();
    }


    @Transactional(readOnly = true)
    public static Result getMovies() {

        List<Property> allMovies = Property.findAll();

        return ok(Json.toJson(allMovies));
    }

    @Transactional(readOnly = true)
    public static Result getMovie(int id) {
        Property property = Property.findById(id);
        return property == null ? notFound() :ok(Json.toJson(property));
    }


}
