package models;

import play.data.validation.Constraints;
import play.db.jpa.*;

import javax.persistence.*;
import java.util.Currency;
import java.util.List;

@Entity
public class Property {


    @Id
    @GeneratedValue
    public Integer id;

    @Constraints.Required
    public String title;

    //    @ManyToOne
//    @JoinColumn(name = "type")
    public String type;

    public String location;

    public Boolean isAvailable;

    public int size;

    public Currency currency;

    public String description;

    public String roomType;

    public int peopleCount;

    public String image;

    public int userId;



    public static List<Property> findAll() {

        TypedQuery<Property> query = JPA.em().createQuery("SELECT m FROM Property m", Property.class);
        return query.getResultList();
    }
    public static Property findById(Integer id) {
        return JPA.em().find(Property.class, id);
    }

    public void save() {

        JPA.em().persist(this);
    }

    public Property update() {
        return JPA.em().merge(this);

    }

    public void delete() {
        JPA.em().remove(this);
    }
}
