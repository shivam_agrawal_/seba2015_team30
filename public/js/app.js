(function(){
    var App = angular.module('propertyCollection', ['ui.router', 'js-data']);

    App.factory('Property', ['DS',function(DS){
        return DS.defineResource('property');
    }]);

    App.config(['$stateProvider', 'DSProvider',function($stateProvider, DSProvider){

        DSProvider.defaults.basePath = '/api';

        var add_new_config = {
            url: '/add_property',
            templateUrl: 'assets/templates/add_new.html',
            controller: ['$scope', 'Property', function($scope, Property){
                $scope.property = Property.createInstance();
                $scope.success = false;

                $scope.save = function() {
                    $scope.property.DSCreate().then(function() {
                        $scope.property = Property.createInstance();
                        $scope.success = true;
                    })
                };
            }]
        };

        var show_all_config = {
            url: '/show_all',
            templateUrl: 'assets/templates/all_Properties.html',
            resolve: {
                properties: ['Property', function(Property){ return Property.findAll();}]
            },
            controller: ['$scope', 'properties', '$state', function($scope, properties,$state){
                $scope.properties = properties;

                $scope.showDetails = function(property) {
                    $state.go('show_all.details', {propertyId : property.id})
                }
            }]
        };

        var property_detail_config = {
            url: '/details/{propertyId}',
            templateUrl: 'assets/templates/property_details.html',
            resolve: {
                movie: ['Property', '$stateParams', function(Property, $stateParams){

                    return Property.find($stateParams.propertyId);
                }]
            },
            controller: ['$scope', 'property', '$state', function($scope, property, $state){
                $scope.property = property;

                $scope.update = function() {
                    movie.DSSave();
                }
                $scope.delete = function() {
                    movie.DSDestroy().then(function(){
                        $state.go('show_all');
                    });
                }
            }]

        }
        $stateProvider.state('add_movie', add_new_config);
        $stateProvider.state('show_all', show_all_config);
        $stateProvider.state('show_all.details', property_detail_config);
    }]);

    App.filter('boolHuman', function(){
        return function(input) {
            return input ? 'Yes':'No';
        }
    });

    App.directive('editText', function(){

        return {
            restrict: 'E',
            scope: {
                value : '='
            },
            require: 'value',
            controller: ['$scope', function($scope){
                $scope.editing = false;
                $scope.toggleEdit = function(save) {
                    if (!$scope.editing) {
                        $scope.editing = true;
                    } else {
                        if (save) {
                            $scope.$parent.update();
                        }
                        $scope.editing = false;
                    }
                }
            }],
            templateUrl: 'assets/templates/edit_text.html'
        };
    });
    App.directive('editBool', function() {
        return {
            restrict: 'E',
            scope: {
                value: '='
            },
            require:'value',
            controller: ['$scope', function($scope){
                $scope.toggleValue = function(){
                    $scope.value = !($scope.value);
                    //seems to be a bug on js-data, needs a new run loop apparently
                    window.setTimeout(function() { $scope.$parent.update();}, 1);

                }
            }],
            templateUrl: 'assets/templates/edit_bool.html'
        };
    });
})()